﻿using HkNotify.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace HkNotify.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}